from django.shortcuts import render
from django.core.exceptions import ValidationError

from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token

from .serializers import UserSerializer
from .serializers import ProfileSerializer
from .serializers import PostSerializer
from .models import User
from .models import Profile
from .models import Post


class UserList(generics.ListCreateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()


class UserProfileUpdate(generics.RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = ProfileSerializer

    def get_object(self, queryset=None):
        # Only get the User record for the user making the request
        try:
            return Profile.objects.get(user__email=self.request.user.email)
        except User.DoesNotExist:
            token = self.request.META['HTTP_AUTHORIZATION'].split(' ')[1]
            return Token.objects.get(key=token).user


class ListCreatePost(generics.ListCreateAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        if self.request.user.is_anonymous:
            return Post.objects.filter(is_public=True)
        return super(ListCreatePost, self).get_queryset()


class RetrieveUpdatePost(generics.RetrieveUpdateAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def get_object(self, queryset=None):
        post = super(RetrieveUpdatePost, self).get_object()
        if self.request.user.is_anonymous:
            if post.is_public:
                return post

        raise ValidationError('Only public posts are accessible for anonymous user.')
