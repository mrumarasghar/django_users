from django.contrib import admin
from django.urls import re_path

from . import views


urlpatterns = [
    re_path(r'users/$', views.UserList.as_view(), name='users_list'),
    re_path(r'user/profile/$', views.UserProfileUpdate.as_view(),
            name='user_profile_update'),
    re_path(r'posts/$', views.ListCreatePost.as_view(), name='posts_list'),
    re_path(r'posts/(?P<pk>\d+)/$', views.RetrieveUpdatePost.as_view(),
            name='post_detail'),
]
