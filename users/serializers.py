from rest_framework import serializers
from django.core.exceptions import ValidationError

from .models import User
from .models import Profile
from .models import Post


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name')


class ProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=False, allow_null=True)

    class Meta:
        model = Profile
        fields = '__all__'

    def update(self, instance, validated_data):
        # user can update onl these fields
        # by using following fields
        # user.first_name, user.last_name
        if 'user' in validated_data:
            user_data = validated_data.pop('user')
            user = User.objects.get(email=instance.user.email)
            user_serializer = UserSerializer(user, data=user_data)
            user_serializer.is_valid(raise_exception=True)
            user_serializer.save()

        super(ProfileSerializer, self).update(instance, validated_data)
        instance = self.Meta.model.objects.get(id=instance.id)
        return instance


class PostSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=False)

    class Meta:
        model = Post
        fields = '__all__'

    def update(self, instance, validated_data):
        if instance.user == self.context['request'].user:
            instance.content = validated_data.get('content', instance.content)
            instance.is_public = validated_data.get('is_public', instance.is_public)
            instance.save()
            return instance
        else:
            raise ValidationError('Only owner of the post can perform this operation.')
