from django.contrib import admin

from .models import User, Profile, Post


class UserAdmin(admin.ModelAdmin):
    pass


admin.site.register(User, UserAdmin)


class ProfileAdmin(admin.ModelAdmin):
    pass


admin.site.register(Profile, ProfileAdmin)


class PostAdmin(admin.ModelAdmin):
    pass


admin.site.register(Post, PostAdmin)